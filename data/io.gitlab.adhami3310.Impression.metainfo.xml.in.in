<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
    <id>@app-id@</id>

    <name>Impression</name>
    <developer_name>Khaleel Al-Adhami</developer_name>
    <summary>Create bootable drives</summary>

    <metadata_license>CC-BY-SA-4.0</metadata_license>
    <project_license>GPL-3.0-only</project_license>

    <description>
        <p>
            Flash disk images onto your drives with ease. Select an image, insert your drive, and you're good to go! Impression is a useful tool for both avid distro-hoppers and casual computer users.
        </p>
    </description>

    <translation type="gettext">@gettext-package@</translation>

    <categories>
        <category>System</category>
        <category>Utility</category>
        <category>GTK</category>
        <category>GNOME</category>
    </categories>

    <keywords>
        <keyword>usb</keyword>
        <keyword>flash</keyword>
        <keyword>bootable</keyword>
        <keyword>drive</keyword>
        <keyword>iso</keyword>
        <keyword>image</keyword>
    </keywords>

    <requires>
        <internet>offline-only</internet>
    </requires>

    <recommends>
        <control>pointing</control>
        <control>keyboard</control>
        <control>touch</control>
    </recommends>

    <screenshots>
        <screenshot>
            <image>https://gitlab.com/adhami3310/Impression/-/raw/main/data/resources/screenshots/0.png</image>
            <caption>Main screen with a chosen ISO and available USB memories</caption>
        </screenshot>
        <screenshot>
            <image>https://gitlab.com/adhami3310/Impression/-/raw/main/data/resources/screenshots/1.png</image>
            <caption>Flashing the ISO in progress</caption>
        </screenshot>
        <screenshot>
            <image>https://gitlab.com/adhami3310/Impression/-/raw/main/data/resources/screenshots/2.png</image>
            <caption>Success screen with a big check mark</caption>
        </screenshot>
    </screenshots>

    <launchable type="desktop-id">@app-id@.desktop</launchable>

    <url type="homepage">https://gitlab.com/adhami3310/Impression</url>
    <url type="bugtracker">https://gitlab.com/adhami3310/Impression/-/issues</url>
    <url type="help">https://gitlab.com/adhami3310/Impression/-/issues</url>
    <url type="vcs-browser">https://gitlab.com/adhami3310/Impression</url>
    <url type="contribute">https://gitlab.com/adhami3310/Impression</url>
    <url type="contact">https://matrix.to/#/@adhami:matrix.org</url>

    <update_contact>khaleel.aladhami@gmail.com</update_contact>

    <content_rating type="oars-1.1" />

    <releases>
        <release version="1.0.1" date="2023-06-07">
            <description>
                <p>Added Spanish, French, German, Russia, and Italian translations.</p>
            </description>
        </release>
        <release version="1.0.0" date="2023-06-03">
            <description>
                <p>Initial version.</p>
            </description>
        </release>
    </releases>
</component>
